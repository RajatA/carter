import 'package:flutter/material.dart';
import './products.dart';

class ProductManager extends StatelessWidget {
  final List<Map<String, dynamic>> products;
  // final Function addProduct;
  // final Function deleteProduct;

  ProductManager(this.products);


  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(children: <Widget>[
        
        Expanded(child: Products(products),)
      ]),
    );
  }
}
