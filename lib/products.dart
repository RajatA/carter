import 'package:flutter/material.dart';
import './pages/product.dart';

class Products extends StatelessWidget {
  final List<Map<String, dynamic>> _products;
  // final Function deleteProduct;

  Products(this._products);

  Widget _buildProductItem(BuildContext context, int index) {
    return Card(
        child: Column(children: <Widget>[
      Image.asset(_products[index]['image']),
      Text(
        _products[index]['title'],
      ),
      ButtonBar(
        alignment: MainAxisAlignment.center,
        children: <Widget>[
          FlatButton(
            child: Text('Details'),
            onPressed: () {
              Navigator.pushNamed<bool>(context, '/product/' + index.toString());
              
            },
          )
        ],
      )
    ]));
  }

  Widget _buildProductList() {
    Widget postCards = Center(
      child: Text('No Product found!'),
    );

    if (_products.length > 0) {
      postCards = ListView.builder(
        itemBuilder: _buildProductItem,
        itemCount: _products.length,
      );
    }
    return postCards;
  }

  @override
  Widget build(BuildContext context) {
    return _buildProductList();
  }
}
