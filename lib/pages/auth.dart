import 'package:flutter/material.dart';
import './products.dart';

class AuthPage extends StatefulWidget {
  @override
  _AuthPageState createState() {
    // TODO: implement createState
    return _AuthPageState();
  }
}

class _AuthPageState extends State<AuthPage> {
  String _emailId, _password;
  bool _acceptTerms = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Login'),
        ),
        body: Container(
          margin: EdgeInsets.all(10.0),
          child: ListView(
            children: <Widget>[
              TextField(
                decoration: InputDecoration(labelText: 'Email id'),
                keyboardType: TextInputType.emailAddress,
                maxLength: 20,
                onChanged: (value) {
                  setState(() {
                    _emailId = value;
                  });
                },
              ),
              SizedBox(
                height: 10,
              ),
              TextField(
                decoration: InputDecoration(labelText: 'Password'),
                keyboardType: TextInputType.text,
                obscureText: true,
                maxLength: 20,
                onChanged: (value) {
                  _password = value;
                },
              ),
              SizedBox(
                height: 10,
              ),
              SwitchListTile(
                value: _acceptTerms,
                onChanged: (bool value) {
                  setState(() {
                    _acceptTerms=value;              
                  });
                },
                title: Text('Accept Terms'),
              ),
              RaisedButton(
                child: Text('LOGIN'),
                onPressed: () {
                  print(_emailId);
                  print(_password);
                  Navigator.pushReplacementNamed(context, '/products');
                },
              ),
            ],
          ),
        ));
  }
}
