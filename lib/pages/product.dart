import 'package:flutter/material.dart';

class ProductPage extends StatelessWidget {
  final String title;
  final String imageUrl;

  ProductPage(this.title, this.imageUrl);

  _showWarningDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Are you sure?'),
          content: Text('This action cannot be undone!'),
          actions: <Widget>[
            FlatButton(
              child: Text('DISCARD'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            FlatButton(
              child: Text('CONTINUE'),
              onPressed: () {
                // below line of code will just close the dialog
                Navigator.pop(context);
                // below line of code will send 'true' to delete the product.
                Navigator.pop(context, true);
              },
            )
          ],
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        print('Back button Pressed');
        Navigator.pop(
            context, false); //false : because we don't want to delete our item!
        // checkout DELETE button functionality for more info
        //If the below call is true, then it will try to execute it's default behavior
        return Future.value(false);
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text('Products detail'),
        ),
        body: Center(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset('assets/food.jpg'),
            Container(padding: EdgeInsets.all(10.0), child: Text('Details')),
            Container(
              padding: EdgeInsets.all(10.0),
              child: RaisedButton(
                color: Theme.of(context).accentColor,
                textColor: Colors.white,
                child: Text('DELETE'),
                onPressed: () {
                  //Navigator.pop(context, true);
                  _showWarningDialog(context);
                },
              ),
            )
          ],
        )),
      ),
    );
  }
}
