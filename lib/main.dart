import 'package:flutter/material.dart';
import './pages/products_admin.dart';
import './pages/products.dart';
import './pages/product.dart';
import './pages/auth.dart';

void main() {
  runApp(Carter());
}

class Carter extends StatefulWidget {

  @override
    State<StatefulWidget> createState() {
      return _CarterState();
    }
}

class _CarterState extends State<Carter>{

  List<Map<String, dynamic>> _products = [];

    void _addProduct(Map<String, dynamic> product) {
    setState(() {
      _products.add(product);
      print('added product');
    });
  }

  void _deleteProduct(int index) {
    setState(() {
      _products.removeAt(index);
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        accentColor: Colors.deepPurple,
        primaryColor: Colors.deepOrange,
        brightness: Brightness.light
      ),
      //home: AuthPage(),
      routes: {
        '/':(BuildContext context) => AuthPage(),
        '/products':(BuildContext context) => ProductsPage(_products),
        '/admin':  (BuildContext context) => ProductsAdminPage(_addProduct, _deleteProduct),
      },
      onGenerateRoute: (RouteSettings settings) {

        final List<String> pathElements = settings.name.split('/'); // /products/1 =>converted to => '','products','1'

        // If the first element is empty string otherwise the ROUTE or the NAME didn't start with a '/' slash
        // In this case we would return a null -> we would fail to route
        if(pathElements[0] != '') { 
          return null;
        }

        if(pathElements[1] == 'product'){
          final int index = int.parse(pathElements[2]);
          return MaterialPageRoute<bool>(
          builder: (BuildContext context) => ProductPage(_products[index]['title'], _products[index]['image'])
        );
        }
        return null;  
        
      },

      onUnknownRoute: (RouteSettings settings) {
        return MaterialPageRoute(
          builder: (BuildContext context) => 
            (ProductsPage(_products)
            )
          );
      },
    );
  }
}
